/*
Copyright © 2023 Tony Jose

*/
package main

import "debfetch/cmd"

func main() {
	cmd.Execute()
}
